
<?php
/**
 * The tutorials template file
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage plain-minimal
 * @since plain-minimal
 */

get_header(); ?>


    <div class="row page-tutorials">
			<div class="col-xs-12 col-md-10 col-md-offset-1">
        <h2>Tutorials</h2>

				<p>Here you can find the recent tutorials.</p>	

				<div class="sub-header">
					<span>Featured</span>
				</div>
				<!-- <h3>Featured</h3>
				<hr class="sm" /> -->

				<div class="row tutorials">
					
					<?php 
					$args = array(
						'post_type' => 'tutorials',
						// 'cat' => '1,3'
					);
					// the query
					$the_query = new WP_Query( $args ); ?>

					<?php if ( $the_query->have_posts() ) : ?>

<!-- pagination here -->

<!-- the loop -->
						<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							<div class="col-xs-12 col-sm-6 tutorial-preview">
								<div class="card">
									<a class="card-link" href="<?php the_permalink(); ?>">
											<!-- check if the post has a Post Thumbnail assigned to it. -->
											<div class="card-thumbnail">
												<?php if ( has_post_thumbnail() ) {
													the_post_thumbnail();
												} ?>
									</div>
									
									<div class="card-text">
										<h2 class="title"><?php the_title(); ?></h2>
										<p class="excerpt"><?php the_excerpt() ?></p>
									</div>
									</a>
							</div>
						</div>
					<?php endwhile; ?>
					<!-- end of the loop -->

					<!-- pagination here -->

					<?php wp_reset_postdata(); ?>

					<?php else : ?>
						<p>Sorry, no posts matched your criteria.</p>
					<?php endif; ?>
				</div>
	   
		
			</div>
    </div> <!-- row -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
