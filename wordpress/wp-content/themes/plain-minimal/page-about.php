
<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


    <div class="row page-about">
			<div class="col-xs-12 col-md-10 col-md-offset-1 text-center">
      <h2><?php echo the_title() ?></h2>
      
      <?php 
        $id=$post->id; 
        $post = get_post($id); 
        $content = apply_filters('the_content', $post->post_content); 
        echo $content;  
      ?>
	   
		
			</div>
    </div> <!-- row -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
