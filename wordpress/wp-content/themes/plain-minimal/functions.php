<?php

// register menus
function register_menus() {
    register_nav_menu('header-menu',__( 'Header Menu' ));
  }
  add_action( 'init', 'register_menus' );
 
// Our custom post type function
function create_tutorial_post_type() {
     register_post_type( 'tutorials',
     // CPT Options
         array(
             'labels' => array(
                 'name' => __( 'Tutorials' ),
                 'singular_name' => __( 'Tutorial' ),
                 'add_new' => __('Add new Tutorial'),
                 'add_new_item' => __('Add new Tutorial'),
                 'edit_item' => __('edit Tutorial'),
                 'update_item' => __('update Tutorial'),
                 'search_item' => __('search Tutorial'),
                 'not_found' => __('Not found'),
                 
             ),
             'public' => true,
             'has_archive' => true,
             'rewrite' => array('slug' => 'tutorial'),
             'supports' => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions'),
         )
     );
 }
 // Hooking up our function to theme setup
 add_action( 'init', 'create_tutorial_post_type' ); 

 wp_enqueue_style( 'style', get_stylesheet_uri() );
 
 // load bootstrap
 function load_bootstrap() {
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery.min.js');
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js');
 }
 add_action('wp_enqueue_scripts', 'load_bootstrap');
 // use custom bootstrap nav walker
 require_once('wp-bootstrap-navwalker.php');

 add_filter('show_admin_bar', '__return_false');

 // support for thumbnails
 add_theme_support( 'post-thumbnails' ); 

?>