

<?php
/**
 * The single tutorial template file
 * @package WordPress
 * @subpackage plain-minimal
 * @since plain-minimal 1.0
 */

get_header(); ?>


    <div class="row single-tutorials">
			<div class="col-md-8 col-md-offset-2 tutorial">
        <?php while ( have_posts() ) : the_post(); ?>
          <h2><?php the_title(); ?></h2>
          <div class="post-content">
            <p><?php the_content(); ?></p>
          </div>

          <!-- get next and previous posts -->
          <div class="tutorial-posts-navigation row">
            <div class="col-xs-12 text-center">
              <div class="previous">
                <?php $prev_post = get_adjacent_post(false, '', true);
                if(!empty($prev_post)) {
                echo '<a href="' . get_permalink($prev_post->ID) . '" title="' . $prev_post->post_title . '">' . 'Previous post: ' . $prev_post->post_title . '</a>'; } ?>
              </div>

              <div class="next">
                <?php $next_post = get_adjacent_post(false, '', false);
                if(!empty($next_post)) {
                echo '<a href="' . get_permalink($next_post->ID) . '" title="' . $next_post->post_title . '">' . 'Next post: ' . $next_post->post_title . '</a>'; } ?> 
              </div>
            </div>
          </div>



              
        <?php endwhile; // end of the loop. ?>	
      </div>
	   
		
    </div> 

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
